package kos.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kos.project.entities.Epic;

public interface EpicRepository extends JpaRepository<Epic, Integer> {

}
