package kos.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kos.project.entities.Story;

public interface StoryRepository extends JpaRepository<Story, Integer> {

}
