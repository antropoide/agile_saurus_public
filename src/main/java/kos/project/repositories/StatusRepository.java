package kos.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kos.project.entities.Status;

public interface StatusRepository extends JpaRepository<Status, Integer> {

}
