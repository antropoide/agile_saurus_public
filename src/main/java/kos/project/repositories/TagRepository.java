package kos.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kos.project.entities.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {

}
