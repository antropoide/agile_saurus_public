package kos.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kos.project.entities.Sprint;

public interface SprintRepository extends JpaRepository<Sprint, Integer> {

}
