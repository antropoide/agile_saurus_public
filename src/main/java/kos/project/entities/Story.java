package kos.project.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="story")
public class Story {
	
	//attributes
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id")
	private int id;	
	@Column
	private String name;
	@Column(name="sequence_order")
	private int sequenceOrder;
	@Column
	private int points;
	@Column
	private String description;
	
	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name= "sprint", referencedColumnName= "id")
	
	private Sprint gotSprint;
	
	@ManyToOne(fetch= FetchType.LAZY) //, cascade= CascadeType.PERSIST)
	@JoinColumn(name= "epic", referencedColumnName= "id")
	private Epic gotEpic;

	@ManyToOne(fetch= FetchType.LAZY)
	@JoinColumn(name= "status", referencedColumnName="id")
	
	private Status gotStatus;
	
	@ManyToMany//(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name= "story_tag",
				joinColumns= @JoinColumn(name= "story_id"),
				inverseJoinColumns= @JoinColumn(name="tag_id"))
	
	private List<Tag> gotTags;
	
	//constructors
	public Story() {
	}

	public Story(String name, int sequenceOrder, int points, String description, Sprint gotSprint, Epic gotEpic,
			Status gotStatus, List<Tag> gotTags) {
		this.name = name;
		this.sequenceOrder = sequenceOrder;
		this.points = points;
		this.description = description;
		this.gotSprint = gotSprint;
		this.gotEpic = gotEpic;
		this.gotStatus = gotStatus;
		this.gotTags = gotTags;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSequenceOrder() {
		return sequenceOrder;
	}

	public void setSequenceOrder(int sequenceOrder) {
		this.sequenceOrder = sequenceOrder;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Sprint getGotSprint() {
		return gotSprint;
	}

	public void setGotSprint(Sprint gotSprint) {
		this.gotSprint = gotSprint;
	}

	public Epic getGotEpic() {

		return gotEpic;
	}

	public void setGotEpic(Epic gotEpic) {
		this.gotEpic = gotEpic;
	}

	public Status getGotStatus() {
		return gotStatus;
	}

	public void setGotStatus(Status gotStatus) {
		this.gotStatus = gotStatus;
	}

	public List<Tag> getGotTags() {
		return gotTags;
	}

	public void setGotTags(List<Tag> gotTags) {
		this.gotTags = gotTags;
	}
	
	

	
}
