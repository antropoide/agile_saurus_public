package kos.project.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Status {

	//attributes
	@Id
	//This field, must have a unique value, but is given by the user
	//May be is not the best strategy... Think about it.
	//@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name= "id")
	private int id;
	
	@Column
	private String status;
	
	@OneToMany(
				mappedBy="gotStatus"
			)
	private List<Story> gotStory;
	
	//constructors
	public Status() {
	}

	public Status(int id, String status, List<Story> gotStory) {
		this.id = id;
		this.status = status;
		this.gotStory = gotStory;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Story> getGotStory() {
		return gotStory;
	}

	public void setGotStory(List<Story> gotStory) {
		this.gotStory = gotStory;
	}

	
	
}
