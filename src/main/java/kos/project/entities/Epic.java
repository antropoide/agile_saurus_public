package kos.project.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="epic")
public class Epic {
	
	//attributes
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name= "description")
	private String description;
	
	@Column(name="starts")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date starts;
	
	@Column(name="ends")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date ends;

	@OneToMany(mappedBy="gotEpic",
			cascade= CascadeType.ALL
				)
	List<Story> gotStory;
	

	
	//constructors

	public Epic() {
	}

	public Epic(String name, String description, Date starts, Date ends, List<Story> gotStory) {
		this.name = name;
		this.description = description;
		this.starts = starts;
		this.ends = ends;
		this.gotStory = gotStory;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStarts() {
		return starts;
	}

	public void setStarts(Date starts) {
		this.starts = starts;
	}

	public Date getEnds() {
		return ends;
	}

	public void setEnds(Date ends) {
		this.ends = ends;
	}

	public List<Story> getGotStory() {
		return gotStory;
	}

	public void setGotStory(List<Story> gotStory) {
		this.gotStory = gotStory;
	}

	
}
