package kos.project.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name= "sprint")
public class Sprint {
	
	//attributes
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)	
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="goal")
	private String goal;
	
	@Column(name="reviewed")
	private Boolean reviewed;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name= "starts")
	private Date starts;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name= "ends")
	private Date ends;
	
	@OneToMany(
			mappedBy="gotSprint" //,
			//cascade= CascadeType.ALL
			)
	List<Story> gotStory;
	

	//constructors
	public Sprint() {
	}


	public Sprint(String name, String goal, Boolean reviewed, Date starts, Date ends, List<Story> gotStory) {
		this.name = name;
		this.goal = goal;
		this.reviewed = reviewed;
		this.starts = starts;
		this.ends = ends;
		this.gotStory = gotStory;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getGoal() {
		return goal;
	}


	public void setGoal(String goal) {
		this.goal = goal;
	}


	public Boolean getReviewed() {
		return reviewed;
	}


	public void setReviewed(Boolean reviewed) {
		this.reviewed = reviewed;
	}


	public Date getStarts() {
		return starts;
	}


	public void setStarts(Date starts) {
		this.starts = starts;
	}


	public Date getEnds() {
		return ends;
	}


	public void setEnds(Date ends) {
		this.ends = ends;
	}


	public List<Story> getGotStory() {
		return gotStory;
	}


	public void setGotStory(List<Story> gotStory) {
		this.gotStory = gotStory;
	}



	
}
