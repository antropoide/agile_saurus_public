package kos.project.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name= "tag")
public class Tag {
	
	//attributes
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name= "tag_name")
	private String tagName;
	
	@ManyToMany(mappedBy= "gotTags")
	private List<Story> gotStories;
	
	//constructors
	
	public Tag() {
	}

	public Tag(String tagName, List<Story> gotStories) {
		this.tagName = tagName;
		this.gotStories = gotStories;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public List<Story> getGotStories() {
		return gotStories;
	}

	public void setGotStories(List<Story> gotStories) {
		this.gotStories = gotStories;
	}
	
	
}
