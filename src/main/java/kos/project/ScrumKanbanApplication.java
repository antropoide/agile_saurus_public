package kos.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrumKanbanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScrumKanbanApplication.class, args);
	}

}
