package kos.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kos.project.entities.Epic;
import kos.project.servicesImplementation.EpicServiceImplementation;

@Controller
@RequestMapping("/epic")
public class EpicController {
	
	EpicServiceImplementation epicServiceImplementation;
	
	@Autowired
	public EpicController(EpicServiceImplementation epicServiceImplementation) {
		this.epicServiceImplementation = epicServiceImplementation;
	}
	
	@GetMapping("/list")
	public String findAll(Model theModel) {
		
		List<Epic> epicList= epicServiceImplementation.findAll();
		
		theModel.addAttribute("theList", epicList);
		
		//Thymeleaf template identifiers
		theModel=templateIds(theModel, "epic", "list");
		
		return "common/list_simple";
	}
	
	
	@GetMapping("/single")
	public String findById(@RequestParam("epicId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		theModel=templateIds(theModel, "epic", "extended data");
			
		return "common/form_read";
	}
	
	
	@GetMapping("/new")
	public String showFormForNew(Model theModel) {
		
		Epic epic = new Epic();
		
		theModel.addAttribute("theObject", epic);
		theModel=templateIds(theModel, "epic", "new record");		
		
		return "common/form_save_update";
	}
	
	@PostMapping("/new")
	public String saveEpic(@ModelAttribute("theObject") Epic epic) {
		
		epicServiceImplementation.save(epic);
		
		return"redirect:/epic/list";
	}
	
	@GetMapping("/update")
	public String udateEpic(@RequestParam("epicId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		theModel=templateIds(theModel, "epic", "update record");		
		
		return "common/form_save_update";
	}
	
	@GetMapping("/delete")
	public  String deletePreview(@RequestParam("epicId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		theModel=templateIds(theModel, "epic", "delete record");		
		
		return "common/form_delete";
	}
	
	@GetMapping("/delete_true")
	public String deleteEpicById(@RequestParam("epicId") int theId) {
		
		epicServiceImplementation.delete(theId);
		
		return "redirect:/epic/list";
	}
	
	//Auxiliary common methods.
	
	public Model templateIds(Model theModel, String theItem, String theAction) {

		theModel.addAttribute("theItem", theItem);	
		theModel.addAttribute("theAction", theAction);
		
		
		return theModel;
	}
	
	private Model populateForm(int theId, Model theModel) {
		
		Epic epic= epicServiceImplementation.findById(theId);
		
		if (epic == null) {
			theModel.addAttribute("theObject", new Epic());
			theModel.addAttribute("mssg01", "No record found");
			return theModel;
		}		
		
		theModel.addAttribute("theObject", epic);
		
		return theModel;
	}	

}






