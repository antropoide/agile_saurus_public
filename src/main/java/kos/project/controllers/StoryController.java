package kos.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kos.project.entities.Story;
import kos.project.servicesImplementation.EpicServiceImplementation;
import kos.project.servicesImplementation.SprintServiceImplementation;
import kos.project.servicesImplementation.StatusServiceImplementation;
import kos.project.servicesImplementation.StoryServiceImplementation;

@Controller
@RequestMapping("/story")
public class StoryController {

	StoryServiceImplementation storyServiceImplementation;
	SprintServiceImplementation sprintServiceImplementation;
	EpicServiceImplementation epicServiceImplementation;
	StatusServiceImplementation statusServiceImplementation;

	@Autowired
	public StoryController(StoryServiceImplementation storyServiceImplementation,
			SprintServiceImplementation sprintServiceImplementation,
			EpicServiceImplementation epicServiceImplementation,
			StatusServiceImplementation statusServiceImplementation) {
		this.storyServiceImplementation = storyServiceImplementation;
		this.sprintServiceImplementation = sprintServiceImplementation;
		this.epicServiceImplementation = epicServiceImplementation;
		this.statusServiceImplementation = statusServiceImplementation;
	}
	
	@GetMapping("/list")
	public String findAll(Model theModel) {
		
		List<Story> storyList= storyServiceImplementation.findAll();
		theModel.addAttribute("theList", storyList);
		theModel=templateIds(theModel, "story", "list");
		//List without tags development needed
		return "common/list_simple";
	}
	


	@GetMapping("/list_tags")
	public String findAllWithTags(Model theModel) {
		
		List<Story> storyList= storyServiceImplementation.findAll();
		theModel.addAttribute("theList", storyList);
		theModel=templateIds(theModel, "story", "list");
		//List without tags development needed		
		return "common/list_simple";
		}
	
	@GetMapping("/single")
	public String findById(@RequestParam("storyId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);		
		theModel=templateIds(theModel, "story", "extended data");
		
		return "common/form_read";
	}
	

	
	@GetMapping("/new")
	public String showFormForNew(Model theModel) {
		
		Story story = new Story();
		
		theModel.addAttribute("theObject", story);
		
		//Catch the default values for data input in form.
		theModel= catchTheFormLists(theModel); 
		theModel=templateIds(theModel, "story", "new record");		
		
		return "common/form_save_update";		
	}
	
	@PostMapping("/new")
	public String saveStory(@ModelAttribute("theObject") Story story) {
		
		storyServiceImplementation.save(story);
		
		return"redirect:/story/list";
	}
	
	@GetMapping("/update")
	public String udateStory(@RequestParam("storyId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		//Catch the default values for data input in form.
		theModel= catchTheFormLists(theModel); 
		theModel=templateIds(theModel, "story", "update record");		
		
		return "common/form_save_update";
	}

	@GetMapping("/delete")
	public  String deletePreview(@RequestParam("storyId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		theModel=templateIds(theModel, "story", "delete record");		
		
		return "common/form_delete";
	}
	
	@GetMapping("/delete_true")
	public String deleteStoryById(@RequestParam("storyId") int theId) {
		
		storyServiceImplementation.delete(theId);
		
		return "redirect:/story/list";
	}
	
	//Auxiliary common methods.
	
	public Model templateIds(Model theModel, String theItem, String theAction) {

		theModel.addAttribute("theItem", theItem);	
		theModel.addAttribute("theAction", theAction);
		
		
		return theModel;
	}
	
	private Model populateForm(int theId, Model theModel) {
		
		Story story= storyServiceImplementation.findById(theId);
		
		if (story == null) {
			theModel.addAttribute("theObject", new Story());
			theModel.addAttribute("mssg01", "No record found");
			return theModel;
		}
		
		theModel.addAttribute("theObject", story);
		
		return theModel;
	}	
	
	private Model catchTheFormLists(Model theModel) {
		
		theModel.addAttribute("theSprint", sprintServiceImplementation.findAll() );

		theModel.addAttribute("theEpic", epicServiceImplementation.findAll());
		
		theModel.addAttribute("theStatus", statusServiceImplementation.findAll());
		
		return theModel;
	}	
		
	
}






