package kos.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kos.project.entities.Sprint;
import kos.project.entities.Status;
import kos.project.servicesImplementation.SprintServiceImplementation;

@Controller
@RequestMapping("/sprint")
public class SprintController {

	SprintServiceImplementation sprintServiceImplementation;

	@Autowired
	public SprintController(SprintServiceImplementation sprintServiceImplementation) {
		this.sprintServiceImplementation = sprintServiceImplementation;
	}
	
	@GetMapping("/list")
	public String findAll(Model theModel) {
		List<Sprint> sprintList= sprintServiceImplementation.findAll();
		
		theModel.addAttribute("theList", sprintList);
		theModel=templateIds(theModel, "sprint", "list");
		
		return "common/list_simple";
	}
	
	@GetMapping("/single")
	public String findById(@RequestParam("sprintId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		theModel=templateIds(theModel, "sprint", "extended data");
		
		return "common/form_read";
	}
	

	@GetMapping("/new")
	public String showFormForNew(Model theModel) {
		
		Sprint sprint = new Sprint();
		
		theModel.addAttribute("theObject", sprint);
		
		theModel=templateIds(theModel, "sprint", "new record");		
		
		return "common/form_save_update";
	}
	
	@PostMapping("/new")
	public String saveSprint(@ModelAttribute("theObject") Sprint sprint) {
		
		sprintServiceImplementation.save(sprint);
		
		return"redirect:/sprint/list";
	}
	
	@GetMapping("/update")
	public String udateSprint(@RequestParam("sprintId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		theModel=templateIds(theModel, "sprint", "update record");		
		
		return "common/form_save_update";
	}
	
	@GetMapping("/delete")
	public  String deletePreview(@RequestParam("sprintId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		theModel=templateIds(theModel, "sprint", "delete record");		
		
		return "common/form_delete";
	}
	
	@GetMapping("/delete_true")
	public String deleteSprintById(@RequestParam("sprintId") int theId) {
		
		sprintServiceImplementation.delete(theId);
		
		return "redirect:/sprint/list";
	}
	
	public Model templateIds(Model theModel, String theItem, String theAction) {

		theModel.addAttribute("theItem", theItem);	
		theModel.addAttribute("theAction", theAction);
		
		return theModel;
	}
	
	private Model populateForm(int theId, Model theModel) {
		
		Sprint sprint= sprintServiceImplementation.findById(theId);
		
		if (sprint == null) {
			theModel.addAttribute("theObject", new Sprint());
			theModel.addAttribute("mssg01", "No record found");
			return theModel;
		}
		
		theModel.addAttribute("theObject", sprint);
		
		return theModel;
	}
	

}


