package kos.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kos.project.entities.Tag;
import kos.project.servicesImplementation.TagServiceImplementation;

@Controller
@RequestMapping("/tag")
public class TagController {

	TagServiceImplementation tagServiceImplementation;

	@Autowired
	public TagController(TagServiceImplementation tagServiceImplementation) {
		this.tagServiceImplementation = tagServiceImplementation;
	}
	
	@GetMapping("/list")
	public String findAll(Model theModel){
		
		List<Tag> tagList = tagServiceImplementation.findAll();
		
		theModel.addAttribute("theList", tagList);
		theModel=templateIds(theModel, "tag", "list");
		
		return "common/list_simple";
	}
	
	@GetMapping("/single")
	public String findById(@RequestParam("tagId") int theId, Model theModel) {
				
		theModel= populateForm(theId, theModel);		
		theModel=templateIds(theModel, "tag", "extended data");
		
		return "common/form_read";
	}
	

	
	@GetMapping("/new")
	public String showFormForNew(Model theModel) {
		
		Tag tag = new Tag();
		
		theModel.addAttribute("theObject", tag);		
		theModel=templateIds(theModel, "tag", "new record");		
		
		return "common/form_save_update";
	}
	
	@PostMapping("/new")
	public String saveTag(@ModelAttribute("theObject") Tag tag) {
		
		tagServiceImplementation.save(tag);
		
		return"redirect:/tag/list";
	}
	
	@GetMapping("/update")
	public String udateTag(@RequestParam("tagId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		theModel=templateIds(theModel, "tag", "update record");		
		
		return "common/form_save_update";
	}
	
	@GetMapping("/delete")
	public  String deletePreview(@RequestParam("tagId") int theId, Model theModel) {
		
		theModel= populateForm( theId, theModel);
		theModel=templateIds(theModel, "tag", "delete record");		
		
		return "common/form_delete";
	}
	
	@GetMapping("/delete_true")
	public String deleteSprintById(@RequestParam("tagId") int theId) {
		
		tagServiceImplementation.delete( theId);
		
		return "redirect:/tag/list";
	}
	
	
	//Auxiliary common methods.	
	public Model templateIds(Model theModel, String theItem, String theAction) {

		theModel.addAttribute("theItem", theItem);	
		theModel.addAttribute("theAction", theAction);
		
		
		return theModel;
	}
	
	private Model populateForm(int theId, Model theModel) {
		
		Tag tag= tagServiceImplementation.findById(theId);
		
		if (tag == null) {
			theModel.addAttribute("theObject", new Tag());
			theModel.addAttribute("mssg01", "No record found");
			return theModel;
		}			
		
		theModel.addAttribute("theObject", tag);
		
		return theModel;
	}
}
