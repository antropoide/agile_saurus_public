package kos.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kos.project.entities.Status;
import kos.project.servicesImplementation.StatusServiceImplementation;

@Controller
@RequestMapping("/status")
public class StatusController {
	
	StatusServiceImplementation    statusServiceImplementation;
	
	@Autowired
	public StatusController(StatusServiceImplementation statusServiceImplementation) {
		this.statusServiceImplementation = statusServiceImplementation;
	}	
	
	@GetMapping("/list")
	public String findAll(Model theModel){
		
		List <Status> statusList = statusServiceImplementation.findAll();
		
		theModel.addAttribute("theList", statusList);
		
		theModel=templateIds(theModel, "status", "list");
		
		return "common/list_simple";
	}
	
	@GetMapping("/single")
	public String findById(@RequestParam("statusId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		theModel=templateIds(theModel, "status", "extended data");
		
		return "common/form_read";
	}
	

	
	@GetMapping("/new")
	public String showFormForNew(Model theModel) {
		
		Status status = new Status();
		
		theModel.addAttribute("theObject", status);
		
		theModel=templateIds(theModel, "status", "new record");		
		
		return "common/form_save_update";
	}
	
	@PostMapping("/new")
	public String saveStatus(@ModelAttribute("theObject") Status status) {
		
		statusServiceImplementation.save(status);
		
		return"redirect:/status/list";
	}
	
	@GetMapping("/update")
	public String udateStatus(@RequestParam("statusId") int theId, Model theModel) {
		
		theModel= populateForm(theId, theModel);
		
		theModel=templateIds(theModel, "status", "update record");		
		
		return "common/form_save_update";
	}
	
	@GetMapping("/delete")
	public  String deletePreview(@RequestParam("statusId") int theId, Model theModel) {
		
		theModel= populateForm( theId, theModel);
		
		theModel=templateIds(theModel, "status", "delete record");		
		
		return "common/form_delete";
	}
	
	@GetMapping("/delete_true")
	public String deleteSprintById(@RequestParam("statusId") int theId) {
		
		statusServiceImplementation.delete( theId);
		
		return "redirect:/status/list";
	}

	//Auxiliary common methods.	
	
	public Model templateIds(Model theModel, String theItem, String theAction) {

		theModel.addAttribute("theItem", theItem);	
		theModel.addAttribute("theAction", theAction);
		
		
		return theModel;
	}	
	
	private Model populateForm(int theId, Model theModel) {
		
		Status status= statusServiceImplementation.findById(theId);
		
		if (status == null) {
			theModel.addAttribute("theObject", new Status());
			theModel.addAttribute("mssg01", "No record found");
			return theModel;
		}
		
		theModel.addAttribute("theObject", status);
		
		return theModel;
	}

}
