package kos.project.servicesImplementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.project.entities.Story;
import kos.project.repositories.StoryRepository;
import kos.project.services.StoryService;

@Service
public class StoryServiceImplementation implements StoryService {

	StoryRepository storyRepository;
	
	
	@Autowired
	public StoryServiceImplementation(StoryRepository storyRepository) {
		this.storyRepository = storyRepository;
	}

	@Override
	public List<Story> findAll() {
		
		List<Story> storyList= storyRepository.findAll();
		return storyList;
	}

	@Override
	public Story findById(int id) {
		
		Optional<Story> storyIn= storyRepository.findById(id);
		
		if(storyIn.isPresent()) {
			Story story= storyIn.get();
			return story;
		}
		
		return null;
	}

	@Override
	public void save(Story story) {
		
		storyRepository.save(story);
	}

	@Override
	public void delete(int id) {
		
		storyRepository.deleteById(id);

	}

}
