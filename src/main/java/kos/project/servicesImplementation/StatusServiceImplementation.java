package kos.project.servicesImplementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.project.entities.Status;
import kos.project.repositories.StatusRepository;
import kos.project.services.StatusService;

@Service
public class StatusServiceImplementation implements StatusService {
	
	StatusRepository statusRepository;
	
	@Autowired
	public StatusServiceImplementation(StatusRepository statusRepository) {
		this.statusRepository = statusRepository;
	}

	@Override
	public List<Status> findAll() {

		List<Status> theStatusList = statusRepository.findAll();
		return theStatusList;
	}

	@Override
	public Status findById(int id) {
		
		Optional<Status> statusIn= statusRepository.findById(id);
		
		if(statusIn.isPresent()) {
			Status status= statusIn.get();
			return status;
		}
		
		return null;
	}

	@Override
	public void save(Status theStatus) {
		
		statusRepository.save(theStatus);

	}

	@Override
	public void delete(int id) {
		
		statusRepository.deleteById(id);

	}

}
