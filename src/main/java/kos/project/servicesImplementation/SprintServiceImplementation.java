package kos.project.servicesImplementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.project.entities.Sprint;
import kos.project.repositories.SprintRepository;
import kos.project.services.SprintService;


@Service
public class SprintServiceImplementation implements SprintService {

	SprintRepository sprintRepository;
	
	@Autowired
	public SprintServiceImplementation(SprintRepository sprintRepository) {
		this.sprintRepository = sprintRepository;
	}

	@Override
	public List<Sprint> findAll() {
		
		List<Sprint> sprintList= sprintRepository.findAll();
		return sprintList;
	}

	@Override
	public Sprint findById(int theId) {
		
		Optional<Sprint> sprintIn= sprintRepository.findById( theId);
		
		if (sprintIn.isPresent()) {
			Sprint sprint = sprintIn.get();
			return sprint;
		}
		
		
		return null;


	}

	@Override
	public void save(Sprint sprint) {
		
		sprintRepository.save(sprint);
	}

	@Override
	public void delete(int theId) {
		
		sprintRepository.deleteById(theId);

	}



}
