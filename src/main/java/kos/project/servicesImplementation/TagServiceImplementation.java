package kos.project.servicesImplementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.project.entities.Tag;
import kos.project.repositories.TagRepository;
import kos.project.services.TagService;

@Service
public class TagServiceImplementation implements TagService {

	TagRepository tagRepository;
	
	@Autowired
	public TagServiceImplementation(TagRepository tagRepository) {
		this.tagRepository = tagRepository;
	}

	@Override
	public List<Tag> findAll() {
		
		List<Tag> tagList= tagRepository.findAll();
		
		return tagList;
	}

	@Override
	public Tag findById(int id) {
		
		Optional<Tag> tagIn= tagRepository.findById(id);
		
		if(tagIn.isPresent()) {
			Tag tag= tagIn.get();
			return tag;
		}
		
		return null;
	}

	@Override
	public void save(Tag tag) {
		
		tagRepository.save(tag);

	}

	@Override
	public void delete(int id) {
		
		tagRepository.deleteById(id);

	}


}
