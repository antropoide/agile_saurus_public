package kos.project.servicesImplementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kos.project.entities.Epic;
import kos.project.repositories.EpicRepository;
import kos.project.repositories.StoryRepository;
import kos.project.services.EpicService;

@Service
public class EpicServiceImplementation implements EpicService {

	EpicRepository epicRepository;
	StoryRepository storyRepository;
	
	@Autowired
	public EpicServiceImplementation(EpicRepository epicRepository, StoryRepository storyRepository) {
		this.epicRepository = epicRepository;
		this.storyRepository = storyRepository;
	}


	@Override
	public List<Epic> findAll() {
		
		List<Epic> epicList= epicRepository.findAll();
		return epicList;
	}

	@Override
	public Epic findById(int id) {
		Optional<Epic> epicIn = epicRepository.findById(id);
		
		if(epicIn.isPresent()) {
			Epic epic = epicIn.get();
			return epic;
		}
		
		return null;
	}

	@Override
	public void save(Epic epic) {
		
		epicRepository.save(epic);
	}

	@Override
	public void delete(int id) {
		
		epicRepository.deleteById(id);
	}


}
