package kos.project.services;

import java.util.List;

import kos.project.entities.Story;

public interface StoryService {
	
	//Basic CRUD functionality
	//define as necessary in each interface.
	public List<Story> findAll();
	public Story findById(int id);
	void save(Story story);
	void delete(int id);
	
	//Other methods

}
