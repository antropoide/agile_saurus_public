package kos.project.services;

import java.util.List;

import kos.project.entities.Epic;

public interface EpicService {
	
	//Basic CRUD functionality
	//define as necessary in each interface.
	public List<Epic> findAll();
	public Epic findById(int id);
	void save(Epic epic);
	void delete(int id);
	
}
