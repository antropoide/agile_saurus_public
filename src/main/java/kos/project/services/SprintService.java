package kos.project.services;

import java.util.List;

import kos.project.entities.Sprint;

public interface SprintService {
	
	//Basic CRUD functionality
	//define as necessary in each interface.
	public List<Sprint> findAll();
	public Sprint findById(int theId);
	void save(Sprint sprint);
	void delete(int theId);
	
	//Other methods

}
