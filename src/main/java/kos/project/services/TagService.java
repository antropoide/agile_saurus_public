package kos.project.services;

import java.util.List;

import kos.project.entities.Tag;

public interface TagService {
	
	//Basic CRUD functionality
	//define as necessary in each interface.
	public List<Tag>findAll();
	public Tag findById(int id);
	void save(Tag tag);
	void delete(int id);
	
	//Other methods

}
