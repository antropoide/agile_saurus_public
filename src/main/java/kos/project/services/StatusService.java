package kos.project.services;

import java.util.List;

import kos.project.entities.Status;

public interface StatusService {
	
	//Basic CRUD functionality
	//define as necessary in each interface.
	public List<Status> findAll();
	public Status findById(int id);
	public void save(Status theStatus);
	public void delete(int id);
	
	//Other methods

}
