
-- 4.4 AWS version (No tabs allowed)
---- Don't copy comments.

drop database if exists my_scrum;
create database my_scrum;
use my_scrum;

--4.4.1 referenced only tables

drop table if exists epic;
create table epic (id int auto_increment not null,
name varchar(32) not null,
description varchar(256),
starts date,
ends date,
constraint epic_PK primary key(id));


drop table if exists status;
create table status(id int not null,
status varchar(32),
constraint status_PK primary key (id)
);


insert into status values
(1,'BACKLOG'),
(2,'TO-DO'),
(3,'DOING'),
(4,'DONE');


drop table if exists tag;
create table tag(
id int auto_increment,
tag_name varchar(64) not null,
constraint tag_PK primary key (id)
);

drop table if exists sprint;
create table sprint(
id int auto_increment,
`name` varchar(32) unique not null,
goal varchar(256),
reviewed boolean default false,
starts date,
ends date,
constraint sprint_PK primary key (id)		
);



--4.4.2 referencing and referenced tables


drop table if exists story;
create table story(
id int auto_increment,
name varchar(128) not null,
sequence_order int,
points int,
sprint int,
epic int,
status int not null,
description varchar(512),
constraint story_PK primary key (id),		
constraint story_to_epic foreign key(epic)
references epic (id)
on delete no action
on update no action,					 
constraint story_to_status_fk foreign key(status)
references status (id)
on delete no action
on update no action,
constraint story_to_sprint_FK foreign key(sprint)
references sprint(id)
on delete no action
on update no action
);

--4.4.3 referencing tables


drop table if exists story_tag;
create table story_tag(
story_id int,
tag_id int,
constraint tag_story_PK primary key (story_id, tag_id),
constraint tag_story_to_story_FK foreign key (story_id)
references story(id)
on delete cascade
on update cascade,
constraint tag_story_to_tag_FK foreign key (tag_id)
references tag(id)
on delete cascade
on update cascade
);


-- 4.4.4 Adding some data

insert into tag values (null, 'tagcheck A'),
(null, 'tagcheck B'),
(null, 'tagcheck C');
select * from tag;


insert into sprint values(null,'A', 'no goal yet', false,'2019-03-01','2019-03-02'),
(null,'C', 'no goal yet', false,'2019-04-01', '2019-04-02'),
(null,'E', 'no goal yet', false,'2019-05-01', '2019-05-02');


insert into epic values(null, 'Lavadoras', 'La colada de la semana','2019-03-01','2019-03-02'),
(null, 'Compras', 'La compra del súper de la semana','2019-03-01','2019-03-02'),
(null, 'Limpieza', 'La limpieza doméstica de la semana','2019-03-01','2019-03-02');


insert into story values (null, 'storyP', 99, 2, 3, 1, 1, 'no desc'),
(null, 'storyO', 999, 2, 1, 2, 2, 'no desc'),
(null, 'storyG', 999, 2, 1, 2, 4, 'no desc'),
(null, 'storyE', 9999, 2, 2, 1, 3, 'no desc');
select * from story;

insert into story_tag values (1,1),
(1,2),
(2,1),
(1,3),
(2,3);


			     

--5 some checking

select name, st.status, description
	from story as sy, status st
	where sy.status= st.id;

----checking many to many
----Get all tag names related to one story

select distinct st.name, ts.tag_name
from
(select story.id, story.name, tag_id
	from story, story_tag
	where story.id = story_tag.story_id
	      ) as st
join
(select tag.tag_name, tag_id
	from story_tag, tag
	where tag_id = tag.id
	      ) as ts
on st.tag_id = ts.tag_id
where st.id = 1
;

-- Backup
\! rm /opt/lampp/htdocs/dbfiles/my_scrum_story.csv;
SELECT * INTO OUTFILE '/opt/lampp/htdocs/dbfiles/my_scrum_story.csv'
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"'
FROM my_scrum.story;

delete from story;
select * from story;
alter table story AUTO_INCREMENT=1;

load data local infile  '/opt/lampp/htdocs/dbfiles/my_scrum_story.csv'
replace into table story
FIELDS TERMINATED BY '|' 
OPTIONALLY ENCLOSED BY '"'
;
select * from story;


--
mysqldump -u root -p my_scrum > /opt/lampp/htdocs/dbfiles/my_scrum.sql
